package com.epam.nickscout;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.*;

public class Main {

    public static final Logger log = LogManager.getLogger();

    public static void main(String[] args) {
        subTaskSleepy();
    }

    public static void subTaskFibbonacci() {
        Random random = new Random();
        int count = random.nextInt(20);
        List<Integer> list = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            list.add(random.nextInt(50));
        }
        log.info(String.format("generated list of integers: %s", list.toString()));
        ExecutorService executorService = Executors.newFixedThreadPool(count);
        List<FibonacciCallable> fibonacciCallables = new ArrayList<>(count);
        list.forEach(integer -> fibonacciCallables.add(new FibonacciCallable(integer)));
        List<Future<Long>> futures;
        try {
            futures = executorService.invokeAll(fibonacciCallables);
            while (!futures.stream().allMatch(integerFuture -> integerFuture.isDone())) { continue; }
            futures.stream().forEachOrdered(integerFuture -> {
                try {
                    log.info(String.format("got F : %d", integerFuture.get()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown();
        }
    }

    public static void subTaskSleepy() {
        int n = 10;
        int l1 = 3;
        int l2 = 3;
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        for (int i = 0; i < 10; i++) {
            service.scheduleWithFixedDelay(new SleepyThread(), l1, l2, TimeUnit.SECONDS);
        }
        service.shutdown();
    }
}

class FibonacciCallable implements Callable<Long> {

    int n;

    public FibonacciCallable(int n) {
        this.n = n;
    }

    @Override
    public Long call() {
        long F = 1;
        long F1 = 1;
        long F2 = 1;

        for (int i = 2; i < n; i++) {
            F = F1 + F2;
            F1 = F2;
            F2 = F;
        }
        return F;
    }
}

class SleepyThread extends Thread {
    int timeToSleep;

    final static int MAX_SLEEP_TIME = 10;
    final static int MIN_SLEEP_TIME = 1;

    public SleepyThread() {
        Random random = new Random();
        timeToSleep = random.ints(MIN_SLEEP_TIME, MAX_SLEEP_TIME)
                .findFirst()
                .getAsInt();
    }

    @Override
    public void run() {
        try {
            Main.log.info(String.format("%s zzz...",getName()));
            wait(timeToSleep);
            Main.log.info(String.format("%s been sleeping for %d!",getName(), timeToSleep));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}